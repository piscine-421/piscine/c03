/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/09/21 12:55:55 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//#include <unistd.h>

int	ft_strncmp(char *s1, char *s2, unsigned int n)
{
	unsigned int	index;

	index = 0;
	if (n == 0)
		return (0);
	while (s1[index] == s2[index])
	{
		n--;
		if (s1[index] == '\0' || n == 0)
			return (0);
		index++;
	}
	return ((unsigned char)s1[index] - (unsigned char)s2[index]);
}

/*int	main(void)
{
	int		returned;
	char	returned2[2];
	char	s1[] = "aaaaaaaa";
	char	s4[] = "aaaaâaaa";

	returned = ft_strncmp(s4, s1, 12);
	if (returned < 0)
	{
		write(1, "-", 1);
		returned *= -1;
	}
	returned2[0] = returned / 10 + 48;
	returned2[1] = returned % 10 + 48;
	write(1, &returned2, 2);
	write(1, "\n", 1);
}*/
