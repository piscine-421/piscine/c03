/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/09/21 12:44:32 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//#include <unistd.h>

char	*ft_strncat(char *dest, char *src, unsigned int nb)
{
	int	index;
	int	index2;

	index = 0;
	index2 = 0;
	while (dest[index])
	{
		index++;
	}
	while (src[index2] && nb > 0)
	{
		dest[index] = src[index2];
		index++;
		index2++;
		nb--;
	}
	dest[index] = '\0';
	return (dest);
}

/*int	main(void)
{
	char	string1[] = "Yo!";
	char	string2[] = " Noid";

	write(1, ft_strncat(string1, string2, 5), 8);
}*/
