/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/09/21 12:44:42 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//#include <unistd.h>

unsigned int	ft_strlcat(char *dest, char *src, unsigned int size)
{
	unsigned int	index[2];
	unsigned int	dest_size;
	unsigned int	src_size;

	index[0] = 0;
	index[1] = 0;
	src_size = 0;
	while (dest[index[0]])
		index[0]++;
	dest_size = index[0];
	while (src[src_size])
		src_size++;
	if (size < dest_size)
		return (size + src_size);
	while (src[index[1]] && size > index[0])
	{
		dest[index[0]] = src[index[1]];
		index[0]++;
		index[1]++;
	}
	if (size > index[0])
		dest[index[0]] = '\0';
	else
		dest[size - 1] = '\0';
	return (dest_size + src_size);
}

/*int	main(void)
{
	char	string1[10] = "Yo! ";
	char	string2[] = "Noid";
	char	returned;

	returned = ft_strlcat(string1, string2, 10) + 48;
	write(1, &returned, 1);
	write(1, &string1, 10);
}*/
