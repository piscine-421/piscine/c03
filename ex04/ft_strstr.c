/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/09/21 12:44:34 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//#include <unistd.h>

char	*ft_strstr(char *str, char *to_find)
{
	int	index;
	int	index2;
	int	index3;

	index = 0;
	index2 = 0;
	if (to_find[0] == '\0')
		return (str);
	while (str[index])
	{
		index3 = index;
		while (str[index3] == to_find[index2])
		{
			if (to_find[index2 + 1] == '\0')
			{
				return (&str[index]);
			}
			index2++;
			index3++;
		}
		index++;
		index2 = 0;
	}
	return (0);
}

/*int	main(void)
{
	char	string1[] = "astring bstring cstring dstring estring fstring";
	char	string2[] = "string";

	write(1, ft_strstr(string1, string2), 9);
}*/
